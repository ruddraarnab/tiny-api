from django.conf.urls import patterns, include, url
from django.contrib import admin
# from user_registration.views import UserLoginView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tinyAPI.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    # url(r'^login', UserLoginView.as_view()),
    url(r'^api-login', 'user_registration.views.login_user'),
    url(r'^api-register', 'user_registration.views.register_user'),
    url(r'^api-logout', 'user_registration.views.logout_user')
)
