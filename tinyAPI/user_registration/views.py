import json
from django.contrib.auth import get_user_model, login, authenticate, logout
from django.contrib.auth.models import User
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework import status, serializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()


@api_view(['POST'])
@csrf_exempt
def register_user(request):
    VALID_USER_FIELDS = [f.name for f in get_user_model()._meta.fields]
    DEFAULTS = {
        # you can define any defaults that you would like for the user, here
    }
    serialized = UserSerializer(data=request.DATA)
    if serialized.is_valid():
        user_data = {field: data for (field, data) in request.DATA.items() if field in VALID_USER_FIELDS}
        user_data.update(DEFAULTS)
        user = get_user_model().objects.create_user(
            **user_data
        )
        return Response(UserSerializer(instance=user).data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)
@api_view(['POST'])
def login_user(request):
    uname= str(request.DATA['username'])
    pword= str(request.DATA['password'])
    user_authenticated= authenticate(username=uname, password=pword)
    if user_authenticated is not None:
        try:
            token= Token.objects.get(user= user_authenticated)
            response_data = {}
            response_data['token'] = str(token)
            response_data['message'] = 'Successfully logged in'
            response_data['success'] = 'True'
            response_data['redirect_url'] = '/'
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        except:
            token= Token.objects.create(user= user_authenticated)
            response_data = {}
            response_data['token'] = str(token)
            response_data['message'] = 'Successfully logged in'
            response_data['success'] = 'True'
            response_data['redirect_url'] = '/'
            return HttpResponse(json.dumps(response_data), content_type="application/json")

    else:
        response_data = {}
        response_data['message'] = 'Login failed'
        response_data['success'] = 'False'
        return HttpResponse(json.dumps(response_data), content_type="application/json")

@api_view(['GET'])
def logout_user(request):
    try:
        logout(request)
        response_data = {}
        response_data['message'] = 'Logged out successfully'
        response_data['success'] = 'True'
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    except:
        response_data['message'] = 'Logout Failed'
        response_data['success'] = 'False'
        return HttpResponse(json.dumps(response_data), content_type="application/json")
